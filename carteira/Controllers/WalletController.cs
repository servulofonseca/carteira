using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.Services;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WalletController : ControllerBase
    {
        private IWalletService _walletService;

        public WalletController(IWalletService walletService)
        {
            _walletService = walletService;
        }


        [HttpGet]
        [Route("amount")]
        [Authorize]
        public IActionResult GetAll()
        {
            var users = _walletService.GetAll();
            return Ok(users);
        }
    }
}
