using System.Text.Json.Serialization;

namespace WebApi.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }

        [JsonIgnore]
        public string Password { get; set; }
        public string Valor { get; set; }
        
        public string History { get; set; }
        public string Datatime { get; set; }
        public string Valorant { get; set; }
        public string Tipo { get; set; }
    }
}