using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models;

namespace WebApi.Services
{
    public interface IWalletService
    {
        IEnumerable<Wallet> GetAll();
    }

    public class WalletService : IWalletService
    {
        
        private List<Wallet> _wallet = new List<Wallet>
        {
            new Wallet { 
                Id = 1,  
                Valor  = "1024.25" }
        };


        private readonly AppSettings _appSettings;

        public WalletService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }


        public IEnumerable<Wallet> GetAll()
        {
            return _wallet;
        }
        


    
     
    }
}